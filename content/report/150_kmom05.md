Kmom05
===============================

###Berätta kort om erfarenheterna med din undersökning av webbplatsers laddningstid.
När det kommer till hur pass snabb sidan upplevs för en användare visste jag att alla tre var rätt långsamma. Det känns som företag, som inte har att göra med teknik gällande datorer, i regel har ganska dåliga webbplatser. Naturligtvis finns det undantag, utan detta är en mer personlig känsla. Biltema presterade bäst men hade ändå rätt dåliga värden för att vara en sådan stor webbplats. Det är relativt lite data som laddas ner jämfört med de övriga två och tiden är klart snabbast.

Clas Ohlson känns rätt likt Biltema i snabbhet och responsivitet. Dock har den sämre värde enligt Google Pagespeed. Förmodligen för att det finns fler resurser som laddas in än Biltema.

Haral Nyborg känns riktigt gammalt i det mesta. Jag tittade runt och så till exempel Jem o fix också hade en dålig hemsida. Det är hur mycket som helst som laddas in samt den upplevs som väldigt seg att navigera igenom.

Det som allihop verkligen behöver fixa är cachelagring samt reducera storleken på deras bilder baserat på skärmstorleken. Även sådana saker som att förminska JavaScript och CSS hade reducerat en laddningstid.

###Har du några funderingar kring Cimage och dess nytta och features?
Jag gillar det mer och mer. Det underlättar väldigt mycket att man slipper skala om bilderna varenda gång man gör en förändring. Det var dock lite struligt att få igång användningen. Jag fick inte igång hur man kunde använda kortare länkar till Cimage mappen först. Visade sig att jag hade totalt glömt lägga till den delen för Cimage i .htaccess. Manualen är väldigt enkel att förstå och är det något specifikt man letar efter är det bara att slå upp det där.

###Lyckades du uppnå ett bra sätt och en LESS-struktur för att jobba med dina bilder i webbplatsen?
Jag har en rätt hyffsad struktur på et mesta nu känner jag. Allt som rör basen för figures ligger i figures.less medans media queries ligger i en som heter media-queries.less. Detta gjorde jag för att samla allt relevant i varje less. Utöver de två så har jag inte spridit ut det mer vad gällande hantering av bilder.

Det är ett smidigt sätt att dela upp det i mindre klasser som man kan lägga till i "FIGURE". Detta reducera kod samt gör det möjligt att kombinera det på flera olika sätt.

###I extrauppgiften om picture, srcset och sizes, fick du någon känsla för för- och nackdelar med konceptet?
Jag har tyvärr inte hunnit läsa igenom de två artiklarna utan jag sparar dem till projektet istället. Sitter parallellt med JavaScript också så känner att jag måste presterar lite på den kursen också.
