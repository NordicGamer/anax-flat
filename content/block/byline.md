<div class="byline-wrapper">
[FIGURE src="image/MeImage.png&w=200" caption="Erik Ståhlberg" class="left noBorder"]
    <p>
        Erik Ståhlberg är utbildad Technical Artist som fortsatt
        läsa programmet Webbprogrammering på Blekinge Tekniska Högskola. Erik spelar
        trumpet på tisdagskvällar i Karlshamns Musikkår samt tycker om att cykla.
    </p>
</div>
