---
author: ersc13
published: "2017-06-20"
category:
    - dagens bild
...
Segling framkallar lugn
==================================

[FIGURE src="image/Sailing.jpg&w=500&h=500" caption="Taget ur en segelbåt vid Tärnö utanför Karlshamn" class="center"]

Segling är något som framkallar lugnet och gör att man njuter av naturen.

<!--more-->

[FIGURE src="image/Sailing.jpg&h=300&area=0,0,0,55" class="right w25"]

Båtens historia {#var1}
-----------------------------------
Min familj har hållit på med fridtidssegling i många år. Det är något som min far började med. Tyvärr sitter han i rullstol nu och har svårt att komma ut. En av mina bröder har tagit hand om båten och håller den i toppskick. Han och min far seglade mycket förr om åren och jag försöker hinna med och hjälpa honom så gott det går.

Varför gillar jag segling? {#var}
-----------------------------------

[FIGURE src="image/Sailing.jpg&w=800&crop-to-fit&area=20,0,32,0&f=colorize,50,0,0,0" caption="Bilden är tagen vid en av bryggorna på Tärnö strax utanför Karlshamn" class="center"]

Det jag personligen gillar med segling är friheten ute i naturen. Det är underbart att äta under en klarblå himmel och hoppa i havet när det är tillräckligt varmt. Jag har aldrig varit motorbåtsintresserad då jag anser att det är inget fridfullt sätt att ta sig ut genom att höra brölet av en motor.



Kamerainställning {#var2}
-----------------------------------
Bilden är tagen med Huawei Y5 II. Det är en mobil med 8MB, f/2, autofokus och led blixt. Inställningen var satt på autofokus och ingen blixt. Ingen annan speciell inställning användes.

[FIGURE src="image/Sailing.jpg&h=500&area=55,0,0,0&f=grayscale" caption="Mycket utsmyckningar över hela katedralen" class="left"]
