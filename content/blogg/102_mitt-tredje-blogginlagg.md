---
author: ersc13
published: "2012-07-30"
category:
    - dagens bild
...
En varm sommardag på Mallorca
==================================

[FIGURE src="image/Mallorca.jpg&w=500&h=500" caption="En liten stad mitt inne i ön" class="center"]

Mallorca är en härlig plats att semestra på. Vare sig du gillar att bada, sola eller åka på turistresor så finns det något för alla.

<!--more-->

Var ligger Mallorca?
-----------------------------------

[FIGURE src="image/Mallorca.jpg&convolve=darken" class="center"]

Staden ligger cirka 25 mil ifrån fastlandet Spanien. Ön har en area på cirka 3640 kvadratkilometer med cirka 870 000 fasta invånare. Det är en oerhört populär turistort med ett antal på cirka 15 miljoner besökare varje år.

Semestra på Mallorca
-----------------------------------

[FIGURE src="image/Mallorca.jpg&convolve=lighten" class="center"]

Jag och min mor var där 2012 på en vecka lång semester. Den veckan var fylld med bad och sol. Vi bodde på ett fint hotell med stranden precis intill grunden. Vi hade det skönt om något för varmt för min smak, närmare 40 grader varje dag. Men man ska verkligen inte klaga.

Dagsutflykter
-----------------------------------

[FIGURE src="image/Mallorca.jpg&crop-to-fit&area=40,25,30,10"" class="center"]

När vi vistades där så åkte vi på två dagsutflykter som vi hade en guide med oss. En av dem var på några undervattensgrottor man kunde gå ner i. Dessa var väldigt stor men tyvärr fick inga bilder tas inne i dem. Den andra utflykten var på två mindre orter och avslutades med en åktur på ett över 100 år gammalt tåg som gick genom bergen. Bilden är tagen på en liten stad ifrån detta tåg med en vanlig digital kamera med autoinställningar.
