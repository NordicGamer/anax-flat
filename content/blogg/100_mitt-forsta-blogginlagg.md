---
author: ersc13
published: "2012-08-16"
category:
    - dagens bild
...

En kyrka med historia
==================================

[FIGURE src="image/Kyrka.jpg&w=500&h=500" caption="Kölnerdomen i sin helhet" class="center"]

Kölnerdomen är en byggnad som har tagit en väldigt lång tid att färdigställa. Det har tagit flera sekler innan den kunde kallas för färdig.

<!--more-->

[FIGURE src="image/Kyrka.jpg&h=500&area=0,0,0,55" class="left w25"]

Hur ser historian ut för Kölnerdomen? {#var}
-----------------------------------

Denna katedral ligger mitt i Köln och är en magnifik kyrka som är relativt välbevarad för att vara så gammal. Den började byggas 1248 men avslutades inte förrän 1880. Detta innebär en byggnadstid på 632 år. Eftersom de båda tornen är på 157 meter utgjorde de byggnaden till världens högsta byggnad i fyra år.

Gamescom? {#var1}
-----------------------------------
Bilden är tagen med en vanlig systemkamera med autoinställningar. Den togs vid en resa till Gamescom, en spelmässa, 2012. Jag och en kompis som var med åkte in till staden efter ett antal timmar på mässan och kom ut ifrån tågbanan där denna katedralen stod.

Detaljrikedom? {#var2}
-----------------------------------

[FIGURE src="image/Kyrka.jpg&h=300&area=30,0,30,55" caption="Mycket utsmyckningar över hela katedralen" class="right w50"]

Kyrkan är väldigt detaljrik då den knappt har en enda rät vinkel i hela byggnaden utan någon form av utsmyckning. Eftersom den är så pass gammal behöver reparationer genomgås i stort sett hela tiden.

[FIGURE src="image/Kyrka.jpg&h=300&area=5,0,60,55" caption="Reperationer pågår för fullt" class="right w50"]
