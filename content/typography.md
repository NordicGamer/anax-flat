Visar typografi
==============================================

#H1-tag
##H2-tag
###H3-tag
####H4-tag
####H5-tag

Här på denna sida visas typografin för denna webbplatsen.
Denna text är vanlig brödtext och nedanför visas styckindelning med lorem ipsum:

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tincidunt enim turpis,
id semper sem tristique ac. Donec feugiat egestas tristique. Nulla consequat nibh ac
ex dapibus, tempus laoreet ante elementum. Nunc a nibh et lorem fermentum tristique
id ac arcu. Proin mollis, orci in dignissim vehicula, justo augue pulvinar nisi, vel
placerat ligula lacus ut purus. Cras congue quam ac neque tempor blandit. Aenean
imperdiet rhoncus dui, in semper ligula euismod id. Nulla mattis nunc nisl, sit
amet ornare lectus placerat at.

Ut ac orci vulputate, ullamcorper velit quis, cursus risus. Vivamus sit amet ligula
egestas, ultrices leo in, vehicula lacus. Aliquam erat volutpat. Nulla at ipsum
tincidunt eros laoreet tristique quis rhoncus tellus. Suspendisse potenti. Donec
dapibus pharetra eleifend. Quisque semper vestibulum risus, eu tempor sem tempus
at. Cras suscipit eleifend ligula. Aenean eu ex sed sapien vestibulum rhoncus.
Nullam congue congue nisl ultrices varius. Nam vitae metus feugiat leo elementum
feugiat vel at ligula. Sed sed eleifend erat. Nam vitae vulputate felis, vitae
laoreet eros. Sed at sodales sapien. Aliquam eu lobortis elit.

**Fetstil ser ut så här.**

*Kursiv stil ser ut på följande sätt.*

***Kursiv och fet stil samtidigt.***

[Såhär ser länkar ut.](#)

En lista kan se ut på följande vis:

+   Hej
+   Hejsan
+   Hallå

eller så här:

1.   Hej
2.   Hejsan
3.   Hallå

Ett kodblock ser ut så här:

```
<?php
    echo "Hello World!";
?>
```

Exempel på block citat med lorem ipsum:

> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tincidunt enim turpis,
id semper sem tristique ac. Donec feugiat egestas tristique. Nulla consequat nibh ac
ex dapibus, tempus laoreet ante elementum. Nunc a nibh et lorem fermentum tristique
id ac arcu. Proin mollis, orci in dignissim vehicula, justo augue pulvinar nisi, vel
placerat ligula lacus ut purus. Cras congue quam ac neque tempor blandit. Aenean
imperdiet rhoncus dui, in semper ligula euismod id. Nulla mattis nunc nisl, sit
amet ornare lectus placerat at.

En tabell kan se ut på följande vis:
| Datum | Uppgift    | Tid   |
| ----- | ---------- | ---   |
| 11/19 | Typografi  | 16:00 |
| 11/20 | Javascript | 15:00 |

Radbrytning:

--------------------------------------------------------------------------------
