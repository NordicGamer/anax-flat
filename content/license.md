Licenses and Terms
==============================================

[Anax Flat är tillgänglig på GitHub](https://github.com/canax/anax-flat) och använder sig av
[MIT lisensen](https://github.com/canax/anax-flat/blob/master/LICENSE).
