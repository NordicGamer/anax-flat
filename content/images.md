Bilder
===============================
Denna sida är till för att testa CImage med "figure" och "figcaption". Lorem Ipsum har använts för att ha en utfyllnadtext. Bilderna använder sedan CImage för att visas.

[FIGURE src="image/Horse.jpg" caption="En häst på Mallorca (Full storlek)" class="center"]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae mollis turpis. Mauris condimentum sollicitudin tincidunt. Praesent varius, nisi sit amet vulputate ultricies, ligula arcu maximus enim, ac pharetra ante tortor nec ligula. Nunc quam ante, efficitur a posuere ac, dapibus ac diam. Vestibulum accumsan tempus ipsum. Sed nec massa pellentesque, vulputate sapien vitae, malesuada magna. Vestibulum consequat nibh eget risus porttitor scelerisque. Cras nec convallis mauris. Mauris magna orci, imperdiet in pulvinar at, gravida suscipit nisl. Quisque fringilla magna ipsum, ac laoreet lacus vulputate accumsan.

Phasellus luctus lorem leo, consectetur molestie orci sollicitudin in. Nulla facilisi. Suspendisse vehicula facilisis purus, a elementum libero imperdiet a. Phasellus commodo eu mauris nec finibus. Aenean volutpat lectus in odio malesuada tempus. Morbi congue ipsum vitae fringilla aliquam. Vestibulum a nunc leo.
flat

[FIGURE src="image/Sailing.jpg&w=300" caption="Segling strax utanför Karlshamn (bredd: 300px; float: left)" class="left w50"]

Fusce laoreet lobortis vulputate. Ut eu augue id purus blandit malesuada. Donec quis euismod quam. Vestibulum in nulla et ex auctor ullamcorper. Donec at dictum neque. Vivamus mauris mi, efficitur at congue nec, egestas vitae quam. Aliquam consectetur justo ut risus egestas, nec sagittis nunc porttitor. Fusce vehicula aliquam sem, vel pellentesque elit tincidunt quis.

Praesent quis odio vitae tortor aliquam aliquam. Sed condimentum placerat tortor, eget pellentesque ante pulvinar eget. Quisque vehicula sapien non arcu tempus, eget finibus velit viverra. Maecenas et condimentum tellus. Suspendisse commodo convallis tempor. Cras sed hendrerit est. Praesent molestie placerat ante. Quisque faucibus placerat ornare. Suspendisse mattis, sem ut placerat faucibus, arcu mauris bibendum sapien, at pellentesque leo turpis ut enim. Proin aliquam nisi justo, eget sagittis felis pulvinar eget. Mauris et porttitor leo. Sed turpis augue, tincidunt vel bibendum vitae, eleifend non dui. Curabitur sit amet gravida augue, efficitur pulvinar urna. Curabitur mattis dapibus tellus id dictum. Proin eu euismod magna.

[FIGURE src="image/KMK-Karlshamn.jpg&w=500" caption="Marchering med Karlshamns Musikkår (bredd: 500px; float: right)" class="right w50"]

Aenean lectus orci, hendrerit id neque eu, vestibulum faucibus elit. Sed vitae mauris at diam euismod cursus. Fusce eu metus a nisi tempus blandit sit amet nec est. Donec ultricies nisl nec nisl cursus dapibus. Ut suscipit bibendum augue, non fermentum nisi ultricies vitae. Nunc vel est eget augue posuere blandit a efficitur leo. Ut rutrum porta nisl sit amet mattis. Vivamus iaculis, nisl a commodo ornare, nulla nibh pellentesque augue, tempor tristique mauris est ut eros. Curabitur lobortis est non laoreet suscipit. Maecenas eleifend, augue eu suscipit commodo, massa diam mollis lectus, a maximus sem justo ac ipsum.

Nulla vitae ante mauris. Aliquam iaculis vulputate nisi sit amet laoreet. Integer et elit vehicula, elementum augue vel, congue sem. Integer nec lectus id diam venenatis porta a sit amet lectus. Morbi nibh tortor, bibendum in ultrices ut, rhoncus auctor erat. Nunc dignissim consectetur molestie. Duis in auctor dolor. Mauris nunc dolor, feugiat in interdum ornare, consequat sed nibh. Phasellus maximus mauris quis lorem pulvinar, vel tincidunt diam luctus.

[FIGURE src="image/Screenshot2.jpg&w=500&r=45&f=negate" caption="Skärmdump på Anax-flat (bredd: 500px; rotering: 45; färg: negate)" class="center"]

Integer nunc est, tristique consequat consectetur dapibus, tempor id risus. Quisque bibendum risus dui, sed iaculis risus fermentum et. Aliquam lorem velit, laoreet vel lacus non, vehicula sodales mauris. Etiam vel nulla quis mi venenatis porttitor sed nec dui. Nam semper vitae ex et imperdiet. Integer tempus est ut velit tincidunt viverra. Phasellus dictum, dolor vel ornare congue, ex urna gravida libero, ac auctor libero justo eget purus. Proin id urna a purus varius pellentesque quis convallis mi. Maecenas mattis tellus euismod urna fringilla, eget sodales magna fermentum. Donec eu augue sed ipsum vehicula interdum quis vitae tellus. Vivamus consectetur, sapien quis lacinia ultrices, dolor dolor condimentum nulla, imperdiet finibus neque lacus vel ipsum. Maecenas in erat vel risus auctor blandit vel pharetra orci. Nam venenatis, augue non aliquet imperdiet, lectus lectus finibus tellus, at aliquam arcu risus vel diam. Pellentesque porttitor libero dapibus sapien euismod, malesuada finibus quam laoreet.

##Skärmdumpar

[FIGURE src="image/Screenshot1.jpg" caption="Skärmdump på arbetet (Full storlek)" class="center"]

[FIGURE src="image/Screenshot2.jpg" caption="Skärmdump på Anax-flat (full storlek)" class="center"]
