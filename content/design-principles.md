Design principer
================

[Här](theme-selector) finner du temaväljaren där det går att byta mellan de olika tema som finns tillgänglig.

##Tema 1
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #1E1E1E"></td>
            <td style="height: 50px; width: 50px; background-color: #2D2D30"></td>
            <td style="height: 50px; width: 50px; background-color: #4a4a4a"></td>
            <td style="height: 50px; width: 50px; background-color: #EBC816"></td>
            <td style="height: 50px; width: 50px; background-color: #84be10"></td>
        </tr>
    </tbody>
</table>

Bakgrundsbilden är en textur med ett repetitivt mönster. Sen för att fullfölja med en annan princip har jag val rörelse i form av länkar. I stort sett alla länkar på webbplatsen har någon form av rörelse. Navbarens ikoner blir rundare, logans text något större och de vanliga länkarna har en animerad understreck.

##Tema2
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
        </tr>
    </tbody>
</table>

Bakgrundsbilden är en bild på en väg. Den visualisera perspektiv med sin långa raksträcka. Sen för att bryta av mot detta valde jag kontrast som min andra princip. Webbplatsen använder sig av svart och vitt som sina huvudfärger. Detta passa bra till vägens mörkgråa färg i bakgrunden.
