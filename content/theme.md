Tema
===============================
Hos [temaväljaren](theme-selector) går det att byta mellan de olika teman som
finns tillgängliga. Jag använde mig till största delen av [Color Scheme Designer 3](http://colorschemedesigner.com/csd-3.5/)
för att skapa monokroma och triadiska färgscheman. För att kunna välja kulörer som
liknar något man redan har, till exempel vid nertryckning av länkar använde jag
[Color-hex](http://www.color-hex.com/). Default temat är satt till den mörka versionen.

##Base
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
        </tr>
    </tbody>
</table>

Bastemat är en väldigt enkelt tema som egentligen inte är tänkt att användas
då temat ligger till grunden för alla de andra. De färger som i stort sett används
här är svart och vitt. Temat fungerar och ger en stilren stil men skulle behöva
någon mer färg för att bryta av själva texten i mitten mot bakgrunden i sidorna.

##Light
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #ccc"></td>
            <td style="height: 50px; width: 50px; background-color: #999"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
        </tr>
    </tbody>
</table>

Detta tema påminner väldigt mycket om bastemat. Det är dock en nyans av grått som
bryter av toppen och botten av webbsidorna samt utanför ramen. Det får en lite mer
inramad känsla vilket medför en mindre känsla att texten inte ligger och flyter.
Eftersom avsaknaden av accentfärg och inga andra färger för den delen används så är temat
monokromt. Jag fick lite inspiration till en annan bakgrundsfärg, utanför ramen,
ifrån lite olika webbplatser. Exempel är [Feber](http://feber.se/), [Biltema](http://www.biltema.se/sv/) mm.

##Color
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #008dc7"></td>
            <td style="height: 50px; width: 50px; background-color: #006994"></td>
            <td style="height: 50px; width: 50px; background-color: #FF9000"></td>
        </tr>
    </tbody>
</table>

Eftersom blå är min favoritfärg så var detta ett lätt val. Jag gillar att bo nära havet
och skulle inte kunna tänka mig att bo mitt inne i landet en längre period. Färgen av
den mörka nyansen kallas "sea blue" och är då väldigt passande att ha. För att bryta
av med en accentfärg valde jag lite mörkare färg av orange. Anledningen är att vanligtvis
är orange eller gul är den typ som används av livräddningsvästar på havet. De är dock väldigt
ljusa och jag ville inte ha en alltför skrikig version av den, därmed tonade jag ner dem en aning.

##Dark
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #1E1E1E"></td>
            <td style="height: 50px; width: 50px; background-color: #2D2D30"></td>
            <td style="height: 50px; width: 50px; background-color: #4a4a4a"></td>
            <td style="height: 50px; width: 50px; background-color: #EBC816"></td>
            <td style="height: 50px; width: 50px; background-color: #84be10"></td>
        </tr>
    </tbody>
</table>

Eftersom jag föredrar mörka teman var denna det första jag utformade. Jag tog
inspiration ifrån [Febers](http://feber.se/) uppbyggnad samt de två första färgerna
ifrån Visual Studios mörka tema. Accentfärgen kommer som inspiration ifrån [IMDb](http://www.imdb.com/).
Själva den gröna färgen används främst till länkar, då sidan börja innehålla en hel del.
Dessutom påminner den gröna färgen den som användes i äldre monitorer förr.

##Colorful
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #022945"></td>
            <td style="height: 50px; width: 50px; background-color: #2D6000"></td>
            <td style="height: 50px; width: 50px; background-color: #6C1D00"></td>
            <td style="height: 50px; width: 50px; background-color: #ffff00"></td>
        </tr>
    </tbody>
</table>

Jag ville få in basfärgerna röd, grön, blå men samtidigt inte överväldiga med starka nyanser.
Därför blev det en aning svagare nyans av dessa tre färger. En accentfärg finns i
färgen gult. Den används precis som i dark varianten och hela temat är baserat därifrån.
Anledningen är då dessa färger passa bra ihop och när jag tänker på någon färg så
är det väldigt ofta dessa tre kommer upp.

##Typography
För att få en lite annorlunda stil på typografin valde jag att ha en separat typsnitt
på själva logan. Det är ett typsnitt som heter "Berkshire Swash". Den sticker ut kraftigt
från de andra vilket är mitt mål. Själva rubrikerna använder en något mjukare variant av
serif-typsnitt. Jag ville få in en mjuk känsla på sidan med det valda temat som är densamma
som color-temat. Dock när det kommer till själva brödtexten ville jag ha en lättläst
text och gick direkt på Roboto, som är stilren och simpel att tyda för de flesta.
