---
titleBreadcrumb: Hem
...
Om mig själv
===============================

[FIGURE src="image/MeImage.png&w=300" caption="Erik Ståhlberg" class="noBorder left"]

Mitt namn är Erik Ståhlberg. Jag är både född och uppvuxen i Åryd, cirka en mil
öster om Karlshamn i Blekinge. Det är en liten plats som har fin natur runt
omkring och ligger cirka 5 minuter cykling ifrån havet. Vi har ingen affär men
konstigt nog har vi en bank. Det sägs att den ska vara bra men själv har jag
ingen aning. Har gått i Åryds skola och sen var det dags att gå över till
högstadiet. Då var man tvungen att åka buss in till den ”stora” staden Karlshamn.

Efter grundskolan var klar var det dags att gå över till gymnasiet lika förvirrad
som de flesta i min ålder valde jag en bred linje. Det blev teknikprogrammet på
Vägga Gymnasieskola i Karlshamn. Programmet var väldigt brett och innehöll det
mesta. När jag väl fick testa på programmering i Java tyckte jag inte alls det
var kul. Förmodligen på grund av en dålig lärare som hela tiden sa: *läs i boken*.

Jag sökte ändå in till BTH i Karlskrona på Technical Artist programmet. I hopp
om att kunna förstå programmering bättre och sitta i Autodesk Maya. Vad jag har
fått lära mig där kan ses på min privata hemsida
[Erik Ståhlberg](http://erikstahlberg.com/). Efter tre år studier och
ett år av uppehåll kände jag för att prova på något nytt och bestämde mig för att
läsa till webbprogrammerare. Än så länge har studierna varit roliga och framförallt
lärorika. Har precis läst klart kurserna python och htmlphp. De har båda gett mig
en lite stadigare grund för hur olika websidor är uppbyggda.

Mina fritidsintressen varierar sig från tid till tid. För tillfället tycker jag
om att ut och cykla långa sträckor, vilket är både avslappnande och får mig att
tänka på något helt annat än programmering och skola. Jag har två äldre bröder
varav den ena tycker om segling så jag bruka följa med honom på somrarna
ibland. Det är min fars båt egentligen men eftersom han blev sjuk och sitter i
rullstol sedan många år tillbaka så har min bror tagit hand om den och mer eller mindre
bestämmer vad som behövs göras och så vidare. Jag spelar sedan flera år tillbaka
trumpet i Karlshamns musikkår. Musiken har varit en hobby som har funnits med mig
sedan långt tillbaka.
