Skrivsätt
===================

##Syfte
Syftet var att göra en analys för en kund om olika webbplatsers struktur på text. Metoden var löst angivet och det valdes att utgå ifrån hur lätt det är att läsa och hitta i olika texter.

##Metod
Denna analys använde sig av "*Skriva för webben*"[^1]. Därifrån togs det 4 olika punkter som ansågs viktiga för skrivning på webben. Dessa är i alfabetisk ordning:

* **Omvänd pyramid** menas att den viktigast information presenteras överst och succesivt avtar till botten.
* **Bilder** är ett viktigt element som säger mer än 1000 ord brukar man säga. Dock när det kommer till webben gäller det inte att ta upp alltför mycket utrymme men samtidigt göra det möjligt att förstora bilderna.
* **Listor** kan spela en stor roll för att lätt hitta i en stor text. Det bör vara en tidigt som går att klicka på och föra en till rätt ställe. Även olika listor i texten kan lätta upp läsandet.
* **Radlängd** bör vara mellan 60-80 tecken långa. Är det mer ökar risken att ögat tröttnar ur och orka inte hänga med. Är den däremot för kort blir texten svårläst då radbyte måste göras väldigt frekvent.

##Val av sidor
Sidorna valdes då de är populära och fokus ligger på information. Det är då flera olika aspekter som bör vara någorlunda bra för att få ett bra flöde i läsandet. Följande sidor utvärderas i denna analys:

* Wikipedia[^2]
* Kickstarter[^3]
* CPPReference[^4]
* Unity dokumentation[^5]

##Analys

###[Wikipedia](https://sv.wikipedia.org/wiki/Telekommunikationer_i_Stockholm)
[FIGURE src="image/wikipedia.jpg?w=615" class="center"]
Wikipedia är en sida där information är dess grund. Här står det mesta inom allt. Fördelen är att den är gratis men nackdelen är att informationen nödvändigtvis inte stämmer då vilken som helst kan skriva här.

####Omvänd pyramid
När det kommer till att presentera det viktigaste först och minst viktigast sist gör Wikipedia detta bra. Innan innehållsförteckningen presenteras artikeln kortfattat. Detta gör det enkelt att avgöra om den är fortsatt värd att läsa.

Efter innehållsförteckningen skrivs oftast historian före nutiden. Detta kan diskuteras om det skulle vara tvärtom men eftersom det går att klicka på länkar till relevanta delar utgör detta inte något större hinder. Längst ner förekommer referenserna precis som tryckt media.

####Bilder
Bilderna som hör till den löpande texten är bra strukturerad. De är oftast inte för stora och inte för små. Önskas en större bild är det bara att klicka på dem. Då visas även lite annan information. Bland annat filnamnet när den skapades osv. Med tanke på att texten utgör en stor del av sidan så fungerar det bra med bilder som utgör en naturlig radbrytning. Även responsivt fungerar detta bra. Bilderna förminskas succesivt i storlek.

####Listor
Efter ingressen kommer mer eller mindre alltid en numrerad lista. Denna innehåller länkar som hoppa till början av vald del. Även listan ute i marginalen till vänster gör det enkelt att byta mellan olika språk, skapa länk eller ta sig till huvudsidan. Speciellt användbart är det med byte av språk då de kan skilja sig rätt mycket i mängd och utformning. Generellt mer innehåll i engelska än svenska.

####Radlängd
Om det är något denna webbplatsen bör få kritik för så är det radlängden. En komplett rad utan något vid kanterna är på ungefär 240 tecken lång. Det är betydligt längre över gränsen om vad som angetts som acceptabelt. Meningen med detta är förmodligen att det är tänkt att kunna ha bilder längst kanterna på båda sidorna. De smälter då ihop med texten men hade varit bättre att ha bilder i marginalen och texten på en max-width. Något som är positivt är dock att texten inte sträcker sig över hela raden och gör mellanrummen mellan orden olika långa.

###[Kickstarter](https://www.kickstarter.com/projects/1049608286/embark-3d-dwarf-fortress-meets-the-sims?ref=discovery)
[FIGURE src="image/kickstarter.jpg?w=615" class="center"]
Denna webbplats är till för att få fram en startkassa till något projekt en person eller företag vill förverkliga. Som privatperson kan då ge en summa pengar mot olika förmåner, beroende på hur mycket han/hon ger och hjälpa till i utvecklingens startkapital.

####Omvänd pyramid
Vid en första anblick ser det relativt bra ut med en kort introduktion överst på varje projekt följt av en video. Längre ner blir det lite sämre då varje projekt kan bestämma hur de vill ha upplägget. Detta kan medföra en obalanserad struktur. Dock är detta något som ligger på den som skriver och inte webbplatsen i sig.

I det stora hela är upplägget bra och den viktigaste informationen ligger överst. Det är en tydlig struktur på texten vilket dessutom medför bra läshastighet.

####Bilder
Som sagt så inkluderas en video överst på varje projekt. Den är i stort sett nyckel i att presentera en idé. Därför ligger den bra till och stör inte ut någon text, då det knappt finns någon just där.

Längre ner är det dock lite värre. Om projektuppläggaren väljer att inkludera bilder så verka de läggas så att de täcker en stor yta. Det som är ännu konstigare är att det finns helt oanvända spalter till vänster och höger. Det hade medfört en bättre flöde i läsandet om bilderna hade förminskats en aning, flyttats till sidorna och gjorts klickbara för att se bilden i naturlig storlek.

####Listor
Efter översta delen finns det en användbar lista med relevanta områden. Kampanjen i sig, FAQ, uppdateringar osv. Detta är ett smart drag för att lätt växla mellan dessa delar. Dock när det kommer till en innehållsförteckning så är avsaknaden stor. Det finns ingen alls. Det är väldigt dåligt då användaren får scrolla genom hela texten och leta manuellt. Listan för de olika mängden pengar en användare kan ge, plus vad han/hon får tillbaka är bra. Varje del är indelat i en egen del med beskrivning.

####Radlängd
Längden på en rad i brödtexten kan sträcka sig upp till cirka 90 tecken. Det är helt acceptabelt och påverka inte ögat alltför mycket negativt vid läsning. Texten är dessutom inte utdragen till att täcka hela rader. Det kan se lite osymmetriskt ut men gör det hela enklare i läsningen då mellanrummen behåller en fast bredd.

###[CPPReference](http://en.cppreference.com/w/cpp/string/basic_string)
[FIGURE src="image/cppreference.jpg?w=615" class="center"]
Denna sida utgör ett bra bibliotek för den som programmerar i C++. Här skrivs olika delar relaterade till standardbiblioteket i C och C++.

####Omvänd pyramid
Strukturen på varje enskild del i en referens kan vara något förvirrande. Det börja med att visa en struktur hur användaren ska inkludera något. Därefter följer en text som förklara vad det är för något. Det skulle behöva göras tydligare med ingressen överst utan kodbitar som enbart förklara innehållet.

Längre ner följer förklaring till variabler och funktioner som denna del är knutet till. Först kommer det som är publikt och kan användas av alla till att övergå till mer interna funktioner/variabler. Med andra ord följer upplägget relativt bra när det kommer till viktigast överst.

####Bilder
Denna webbplats innehåller inga bilder utan inriktar sig enbart på ren programmering. Dock är det kodbitar som visas då och då. De täcker oftast hela bredden av texten och gör det tydligt att läsa. I det här sammanhanget kan det vara en fördel då det är ofta det användaren är ute efter.

####Listor
Det första man möts av är en lista med alla referenser som inkluderas på denna webbplatsen. Det utgör en bra grund till att hitta det användaren söker efter. Väl inne på ett ämne är det dock lite rörigare. Det finns ingen lista som beskriver de olika rubrikerna. Det kan ha att göra med att varje ämne är relativt korta.

Listor används för att bryta av texten naturligt och ger en bättre översikt om vad som beskrivs. Den delen är bra uppbyggd.

####Radlängd
Radlängden ligger på ungefär 115 tecken. Det är lite för långt men går ändå att läsa utan större svårigheter. Dock är det en rätt liten typsnitt som används så bredden upplevs som mindre. Återigen behåller texten sin fasta bredd mellan orden, vilket är bra.

###[Unity dokumentation](https://docs.unity3d.com/Manual/Retargeting.html)
[FIGURE src="image/unity.jpg?w=615" class="center"]
Denna webbplats innehåller dokumentation för Unity 3D. Det är en spelmotor med många delar att hålla reda på. Därför är det av stor vikt att det är lätt att hitta och läsa lätt.

####Omvänd pyramid
Varje ämne börjar med en kort beskrivning vad det handlar om. Sedan följt oftast med det en nybörjare söker efter. Sist står till exempel optimeringar, vilket är mer för den erfarne utvecklaren. Varje ämne är relativt kort men utgår i stort sett från liknande strukturer på allihop.

####Bilder
Bilder bryter av texten rätt abrupt. Texten smälter inte ihop med bilderna utan de är separata från varandra. Det hade varit bättre att korta ner radlängden och läggar in bilderna till höger. Det hade get en bättre lässtruktur med bilder vid sidan. De hade dessutom kunnat förminskats och gjort möjligt att klicka på för att få en större version.

####Listor
Listor används i stora drag. För en sådan här webbplats är det bra. Det kan ge en klarare bild vad ämnet handlar om eller en steg för steg beskrivning. Hursomhelst är de till stor nytta här. Något som kunde förbättrats är att gjort ett litet indrag så att de inte ligger i samma vertikala linje som övrig text. Det hade gjort dem en aning lättare att hitta.

####Radlängd
Längden är cirka 160 tecken lång vilket är alldeles för mycket. Precis som det nämndes innan skulle det passat bättre med bilderna i högra sidan och texten kortas ner.

##Sammanfattning
Överlag verka det vara en tendens till alldeles för långa rader. Dessutom är bilderna oftast för stora och inte förstoringsbara. Det hade varit att föredra någon version av till exempel CImage[^6] eller liknande. Dessutom hade det känts bättre att sätta bilder i sidan om texten för att inte bryta av helt och hållet.

Listor används flitigt på dessa sidor och det är i stort sett nödvändigt för att förklara något. Det är dock bara Wikipedia som har någon form av innehållsförteckning på varje artikel/ämne. Kickstarter hade verkligen behövt det också. De övriga två har oftast väldigt korta ämnen och har inte lika stort behov av det.

Överlag är det relativt enkelt för användaren att finna det han/hon söker. Som sagt bilderna behöver tänkas om och radlängden kortas ner.

[^1]: Skriva för webben. IIS förstasida. https://www.iis.se/lar-dig-mer/guider/hur-man-skriver-for-webben/ 2018-01-01. [Länk](https://www.iis.se/lar-dig-mer/guider/hur-man-skriver-for-webben/).
[^2]: Wikipedia. Telekommunikationer i Stockholm. https://sv.wikipedia.org/wiki/Telekommunikationer_i_Stockholm 2018-01-01. [Länk](https://sv.wikipedia.org/wiki/Telekommunikationer_i_Stockholm).
[^3]: Kickstarter. Embark - 3D Dwarf Fortress meets The Sims. https://www.kickstarter.com/projects/1049608286/embark-3d-dwarf-fortress-meets-the-sims?ref=discovery. 2018-01-03. [Länk](https://www.kickstarter.com/projects/1049608286/embark-3d-dwarf-fortress-meets-the-sims?ref=discovery).
[^4]: CPPReference. basic_string. http://en.cppreference.com/w/cpp/string/basic_string. 2018-01-03. [Länk](http://en.cppreference.com/w/cpp/string/basic_string).
[^5]: Unity dokumentation. Retargeting of Humanoid animations. https://docs.unity3d.com/Manual/Retargeting.html. 2018-01-04. [Länk](https://docs.unity3d.com/Manual/Retargeting.html).
[^6]: CImage. Förstasidan. https://cimage.se/. 2018-01-04 [Länk](https://cimage.se/).
