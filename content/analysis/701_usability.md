Användarvänlighet
================

##Syfte
Denna analys av olika webbplatsers användarvänlighet är gjord för en kund. Kunden ville veta vad som anses som en bra webbplats baserad på artikeln "*Usability 101: Introduction to Usability*"[^1].

##Metod
Denna analys använde sig av "*Usability 101: Introduction to Usability*"[^1] för att utvärdera de fem olika aspekterna som utgör en bra webbplats. De följande punkterna är det som utvärderas:

* Lärbarhet
* Effektivitet
* Minnesvärdhet
* Fel
* Tillfredsställelse

Varje punkt utvärderas oberoende av varandra för att ge en sådan pass rättvis bedömning som möjligt. För varje punkt har en poängskala 1-5 används där 1 är lägsta möjlig poäng och 5 högsta.

##Val av sidor
De sidor som har valts att utvärdera är följande:

* Youtube[^2]
* Feber[^3]
* Google[^5]
* Unreal Engine 4 Dokumentation[^6]

Dessa sidor är sådana jag personligen använder dagligen.

##Analys

###Youtube
[FIGURE src="image/Youtube.jpg?w=615" class="center"]
[Youtube](https://www.youtube.com/) är en av de sidor jag använder mest och vill därmed att den ska vara snabb och lätt att använda.

####Lärbarhet (5)
Vid första anblick på sidan dras gärna blicken upp till loggan i övre vänstra hörnet. Jämte den är sökfältet och därmed är det lätt att hitta en av de viktigaste delarna av sidan. Att kunna söka efter filmklipp är det som Youtube använder sig av mest. En annan del som sidan tilldelar så är det slumpmässiga filmklipp på förstasidan. För användaren är det väldigt enkelt att börja titta på något. Antigen söker han/hon efter något eller klicka på urvalet mitt i sidan.

####Effektivitet (4)
Eftersom det inte är någon hög inlärningskurva tar det inte lång tid att utföra olika kommandon. För att söka är det bara att klicka i sökfältet och skriva. Något som kan vara en aning avigt är filtrering på filmklippen. Den dyker bara upp efter att man har sökt. Det är även relativt enkelt att lägga upp egna videos. Uppe i högra hörnet finns en speciell uppläggningsknapp. Denna är till för att snabbt lägga upp egna videos. Klickar användaren på den kommer han/hon till en sida där man antigen kan välja från fil eller klicka och dra in en video från sin dator. Mycket enklare går det inte att göra.

####Minnesvärdhet (4)
Eftersom Youtube är så pass enkel att använda är det enkelt att komma tillbaka en tid senare. Användaren kommer säkerligen ihåg de mest triviala kommandon. Det som drar ner poängen en aning är att just designmässigt ändras Youtube då och då. Det kan ställa till det om en person kommer tillbaka efter ett tags uppehåll. Dock är det oftast till det bättre som både design och användarvänligheten ändras. Nya funktioner och bättre designval görs oftast.

####Fel (5)
Eftersom denna webbplatsen är riktad mot att visa och lägga upp filmklipp så är det ett begränsat antal kommandon som kan göras. Detta medför i sin tur ett litet antal fel som kan göras. För att ta ett exempel, väljer någon att lägga till ett filmklipp i "titta senare" kategorin och vill sedan ta väck det så klickar han/hon bara på ta bort knappen på videon och den tas väck. Liknande enkla åtgärden kan göras på de flesta saker och gör det därmed enkelt att korrigera olika fel.

####Tillfredsställelse (5)
Webbplatsen är som sagt enkel uppbyggd och har därmed en enkel design. Det förekommer inga element som inte ligger i det tydliga gridet och det är i detta fallet enbart en bra poäng. Det finns inte så mycket att diskutera som borde bli bättre i denna aspekten. Under varje filmklipp följs kommentarer och de är sorterade efter hur många som gillar den. Det som har lagts till på senaste tiden är kategorin "Historik" och den finns väldigt lättillgängligt till vänster i en meny där den går att dölja med ett klick.

####Summering (23/25)
Denna webbplats är väldigt enkel att använda och designmässigt är den väldigt bra upplagd. För en ny användare är det enkelt att direkt börja använda och är det någon som vill gå lite djupare och lägga upp egna filmklipp, samt redigera, så finns det stöd för det också. Sidan får därför 23 av 25 i betyg.

###Google
[FIGURE src="image/google.jpg?w=615" class="center"]
[Google](https://www.google.se/) är en väldigt stor webbplats och bör därmed vara enkel att använda. Något som känneteckna sidan är en stilren layout.

####Lärbarhet (5)
Sidan för sökning är så pass enkel att det går i stort sett inte att göra fel.
Ett sökfält är i stort sett det enda som finns. Poängen är att skriva in en söksträng och klicka på knappen eller enter. Vill användaren söka på bilder, nyheter, filmklipp mm, finns det möjlighet till det strax under sökfältet. Alla relevanta träffar listas därefter rakt under.

####Effektivitet (5)
Eftersom det är en väldigt låg upplärning och en enkel webbplats är det därmed lätt att utföra olika kommandon. Sökning sker naturligt och behöver användaren begränsa eller lista om sina träffar finns det enkla val för det också. För att komma till olika delar finns de oftast bara några få knapptryck bort som känns naturligt.

####Minnesvärdhet (5)
Eftersom sidan är begränsad med i stort sett ett enda fokus så utgör det en stark minnesvärdighet. Det är därmed i stort sett inga svårigheter för att komma ihåg efter en längre. Det de flesta användare behöver finns i stort sett under sökfältet. Det är därmed lätt att komma ihåg.

####Fel (4)
Det finns inga direkt problem som kan återstå vid fel sökning. Är det så att användaren har slagit in någon restriktion så meddelas detta under sökfältet.
Det som kan vara ett problem är sidor som försöker locka till sig besökare. De kan ha malware eller liknande. Dock ligger inte detta på Google i sig utan på sidan själv. Det som Google försöker göra är att varna användaren för misstänkta sidor.

####Tillfredsställelse (3)
Här är den största nackdelen med Google. Det är en enkel sida med tydligt upplägg. Dock fallerar den rätt bra på historiken. Efter en be4sökt sida så ändra länken färg men det är knappt urskiljbart. Det är en riktigt dålig del och borde rättas till en betydligt tydligare färg. Något annat som borde finnas som alternativ är ett "dark mode" som till exempel Youtube. Det finns många som skulle vilja ha ett sådant val, då de flesta program i nuläget stödjer det.

####Summering (22/25)
Google är verkligen ingen dålig webbplats och borde vara ett första val i sökmotor. Den har lite problem med designen och speciellt när det kommer till länkarna. Annars finns det i stort sett inte mycket mer att säga.

##Feber
[FIGURE src="image/Feber.jpg?w=615" class="center"]
[Feber](http://feber.se/) är en populär sida för teknikintresserade inom Sverige som ägs av Bonnier AB[^4]. Den agera som en nyhetssida med ett ständigt flöde med teknikrelaterade ämnen.

####Lärbarhet (3)
För en ny användare är det väldigt enkelt att sätta igång och läsa direkt. Varje ny artikel läggs överst och har det skrivits om liknande eller om samma sak så länkas de oftast i artikeln för en snabbare åtkomst. Det som drar ner betyget är bland annat kommentarfältet, sökfunktionen och allmänt var olika menyer befinner sig. Kommenterarna måste klickas på en knapp, som inte är alltför tydlig, för att visas. Sökfältet är inte alls tydlig var den befinner sig och vid sökning kommer användaren till en interagerad Google-sida. med förvirrad layout. Det saknas dessutom helt att kunna sortera artiklar.

####Effektivitet (3)
Som det nämndes tidigare är det ett förvirrat upplägg i vissa aspekter. Något som kan uppfattas som irriterande är att kommentarerna måste laddas för varje artikel och inte för varje sida, vilket borde vara mer logiskt. Vill användaren också hitta en artikel med sökord kan det vara en aning svårt då det knappt finns någon form av filtrering. De som finns är relevans och datum. Dock något som kan vara bra är visning av de mes populäraste artiklarna. Den funktionen är lättåtkomlig genom ett knapptryck bort överst på sidan.

####Minnesvärdhet (5)
Sidan har inte ändrat layout som förvirra användarna på väldigt länge. Detta medför att användaren med största sannolikhet kan återvända till Feber efter ett tag av uppehåll och ändå få ut det mesta. Eftersom de flesta saker i artiklarna är tydligt uppmärkta så gör det att nya och gamla användare kan klicka sig vidare relativt enkelt.

####Fel (4)
Det finns en del saker som kan uppstå. Råka användaren klicka på reklamen som finns mellan olika artiklar tas han/hon till en ny sida. Det kan vara en aning enerverande men går snabbt att korrigera genom att stänga ner fliken. Har användaren råkat klicka på fel håll med poängsystemet för artikeln kan detta inte åtgärdas lätt genom att klicka på motsatt håll. Utöver dessa är det inte så mycket annat som kan uppstå.

####Tillfredsställelse (5)
Upplägget är väldigt enkelt i grunden och är därmed lätt att använda. Rent designmässigt är det enkla färger och typsnitt. Hela sidan går ut på att läsa och betygsätta olika nyheter. Det framkommer på flera ställen och är lätt att både använda och hitta i designen.

####Summering (20/25)
I det stora hela är sidan relativt enkel och bra strukturerad. Det finns några aspekter som hade kunna bli bättre med användarvänligheten men rent designmässigt är den bra. Webbplatsen får därmed 20 av 25 i betyg.

###Unreal Engine 4 Dokumentation
[FIGURE src="image/UE4.jpg?w=615" class="center"]
[Unreal Engine 4 Dokumentation](https://docs.unrealengine.com/latest/INT/) riktar sig till ett bibliotek av dokumentation till spelmotorn Unreal Engine 4. Den uppdateras hela tiden med nya och relevanta ämnen.

####Lärbarhet (4)
Det kan se lite rörigt ut vid första anblick. Dock är sidan bra strukturerad för att vara en dokumentation över en hel spelmotor. Till vänster är olika delar av motorn och mitt på sidan finns sökfält och olika ämnen. Det som kan ta lite tid är att hitta just det man söker efter och sökfunktionen är inte världens bästa men gör sitt jobb ändå.

####Effektivitet (4)
Eftersom det är lite uppdelat kan det ta lite tid att hitta. Har användaren dock hittat det han/hon söker efter brukar det inte vara några större svårigheter att hitta relevanta områden därifrån. Är det en helt nya användare finns det relevanta video guider och även skrivna guider. Finns det inte i dokumentationen går det snabbt att klicka över till gemenskapskanalen överst på sidan.

####Minnesvärdhet (2)
Här kan det fallerar relativt snabbt eftersom det finns ingen historik eller annan form av sparning. Det enda sättet att veta exakt var man var någonstans när man lämnade sist är genom bokmärken. En annan sak är att eftersom motorn uppgraderas med jämna mellanrum förändras också dokumentationen. Det enda som håller det någorlunda positivt är just att layouten inte förändras med tiden utan håller sig till samma stil.

####Fel (4)
Det går inte göra särskilt mycket fel på denna webbplats mer än att klicka på fel länk. Detta åtgärdas genom antigen backa, söka igen eller klicka på någon annan länk. En annan bra sak är att om användaren har en vag ide vad han/hon är ute efter och hittar något som påminner om det, visas oftast relevanta länkar i texten. Detta medför att användaren inte nödvändigtvis behöver veta exakt vad det man letar efter heter.

####Tillfredsställelse (5)
För att vara en dokumentation är den varken tråkig att titta på eller använda. Den känns modern och uppdaterad. Det finns småsaker som gör allting mycket enkelt att använda. Ett exempel är knappen för att skifta mellan Windows och Mac OS dokumentationen. Allting ser lika dant ut men innehåller ändras till att passa operativsystemets upplägg. Det finns andra fördelar som att sortera efter svårighetsgrad, vilket inte känns särskilt vanligt i en dokumentation.

####Summering (19/25)
Överlag är det en bra sida för ändamålet. Kan vara något knepig att hitta i men för att se det från ett annat perspektiv så innehåller den mycket information. Något sätt att spåra sina sökningar och spara till en historik hade varit att föredra. Layouten är dock väldigt bra och bör hålla ett bra tag framöver.

[^1]: Nieelsen Norman Group. 2012-01-04. https://www.nngroup.com/articles/usability-101-introduction-to-usability/ 2017-12-21. [Länk](https://www.nngroup.com/articles/usability-101-introduction-to-usability/).
[^2]: Youtube. Youtube förstasida. 2017-12-21. https://www.youtube.com/ 2017-12-21. [Länk](https://www.youtube.com/).
[^3]: Feber. Febers förstasida. 2017-12-21. http://feber.se/ 2017-12-21. [Länk](http://feber.se/).
[^4]: Bonnier. Bonnier förstasida. https://www.bonnier.com/sv/ 2017-12-22. [Länk](https://www.bonnier.com/sv/).
[^5]: Google. Förstasidan. https://www.google.se/ 2017-12-22. [Länk](https://www.google.se/).
[^6]: Unreal Engine 4 Dokumentation. Förstasidan. https://docs.unrealengine.com/latest/INT/ 2017-12-23. [Länk](https://docs.unrealengine.com/latest/INT/).
