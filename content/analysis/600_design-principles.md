Design principer
================

##Syfte
Syftet med denna undersökning var att fastställa hur olika webbplatser använder sig av olika design principer samt olika element. Undersökningen utfördes på tre olika webbplatser i följande ordning: Steam[^1], Google[^2], Netflix[^3].

##Metod
Metoden för att kunna utvärdera dessa sidor har gjorts genom användning av "The Principles of Beutiful Web Design, Third Edition"[^4] och "Design Elements & Principles"[^6]. Även verktyget "WhatFont"[^7] har använts för att ta reda på vilken typsnitt som används.

##Val av sidor
De sidor som blev utvalda till denna undersökning baseras på min egen användning. När jag använder dessa sidor är jag ute efter ett snabbt resultat och vill gärna finna det jag söker så snabbt som möjligt. Sökning och övervägande visuellt bör därmed vara stabilt för att finna det användaren söker efter.

##Analys

###Steam
[FIGURE src="image/steam.jpg?w=615" class="center"]
[Steam](http://store.steampowered.com/) är en av de största när det kommer till digital distribution av datorspel. Eftersom den innehåller så pass många olika spel är av vikt att det är lätt att hitta det sökaren är ute efter.

Steam har en rätt stilren hemsida men ändå en del färg på det hela. Den känns relativt enkel att hitta det man söker efter. Dock är avsaknaden av en ram till vänstra navbaren en miss enligt mig. Det känns som den flyter ihop med själva visningen av de olika spelen.

Den designprincip som används helt klart mest på denna webbplats är en blandning av grid och hierarki. Alla element ligger tydligt i gridet men är dessutom sällan i samma storlek. Allt handlar om att synas först för att dra till sig användarens blick. Detta gör att skalan på varje element också spela roll i synlighet. Steam är anpassad till varje användare med relevanta förslag och ger därför olika spel olika mycket utrymme och storlek.

Personligen tycker jag stilen passar hemsidan och allt är rätt lätt att hitta. Det mesta finns under någon av menyerna om de inte visas i det stora fältet i mitten.

###Google
[FIGURE src="image/google.jpg?w=615" class="center"]
[Google](https://www.google.se/) är nog den största söksida som finns i nuläget. Därmed ligger det mycket ansvar för att vara lättnavigerad och snabb. Det som känneteckna sidan är en stilren layout med i stort sett enbart ett sökfält och sökträffar.

Google använder sig av tydliga linjer, någon form av grid, för att strukturera upp sökningen. Eftersom den använder sig till största delen av vänstra halvan av skärmen gör det lätt att skala ner den till mobil form. Sidan är mer eller mindre gjord för text och använder därmed sig av Arial vilket är en säker font för webben[^5]. När det kommer till färg har de ett monokromt färgschema på länkarna, där det största fokus ligger.

Sidan använder sig till största del av ett grid som princip i form av en lista. Detta skulle kunna sträcka sig till repetition eftersom varje sökförslag ser likadan ut i formen men skiljer sig i innehållet.

Personligen tycker jag sidan är väldigt bra förutom på en punkt. Färgen på vanliga länkar och besökta är i stort sett ingen skillnad. Detta har gjort att jag installerat ett plugin som färger dem rött vid besökt, vilket inte borde behövas.

###Netflix
[FIGURE src="image/netflix.jpg?w=615" class="center"]
[Netflix](https://www.netflix.com/) fungerar som en enda stor samling med filmer och serier. Detta gör att sökning och visning av relaterade filmer och serier behöver vara väldigt bra för att kunna hålla kvar kunden ifrån konkurrenterna.

Det som känneteckna Netflix är en enkel layout över hela webbplatsen. Antagligen för att det ska passa till olika enheter. Speciellt anpassat känns det till tv då det är enkelt att navigera och starta en film eller serie. Det finns ytterst få alternativ att göra. Detta valet har antagligen gjorts då det inte behövs särskilt mycket fler valmöjligheter än valet av språk på ljud och undertexter.

Återigen är grid det som används mest men fokusen ligger mycket på texturer eller bilder för att locka användaren till en serier eller film. Dock är det en annan väldigt viktig design princip som används för att ge förslag. Det handlar om skala och rörelse. Det första en besökare bemöts av, om han/hon har ett konto, är en heltäckande minivideo av en passande film eller serie. Även vissa filmer och serier börja spela en trailer när användaren läser den övergripande texten.

Överlag passar denna designen väldigt bra för Netflix. Det som är bra med en är att sidan ser i stort sett lika dan ut oavsett vilken enhet man använder.

##Avslutningsvis
Denna analys av webbplatsers design principer är genomförd av Erik Ståhlberg i kursen [Teknisk webbdesign och användbarhet](https://dbwebb.se/kurser/design). De olika undersökningarna är till största del personlig analys och kan variera från person till person.

[^1]: Steam. Förstasidan. http://store.steampowered.com/ 2017-12-09.
[^2]: Google. Förstasidan. https://www.google.se/ 2017-12-09.
[^3]: Netflix. Förstasidan. https://www.netflix.com/ 2017-12-09.
[^4]: Beaird, Jason. (2014). The Principles of Beutiful Web Design. Third Edition. SitePoint.
[^5]: w3school. Web Safe Font. https://www.w3schools.com/cssref/css_websafe_fonts.asp 2017-12-09.
[^6]: Design Elements & Principles. Förstasidan. https://www.canva.com/learn/design-elements-principles/ 2017-12-15.
[^7]: WhatFont. https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm 2017-11-02.
