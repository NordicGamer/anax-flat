Om denna webbplats
==============================================

[FIGURE src="image/about.jpg?w=256" class="right"]

Denna webbplats är till för kursen [design](http://dbwebb.se/design) och hålls
på Blekinge Tekniska Högskola (BTH), i Karlskrona.
Själva koden som används ör att skapa denna sidan finns tillgänglig på [Github](https://github.com/NordicGamer/Anax-Flat/).
