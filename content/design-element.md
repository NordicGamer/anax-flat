Design element
==============

[Här](theme-selector) finner du temaväljaren där det går att byta mellan de olika tema som finns tillgänglig.

##Tema 1
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #555130"></td>
            <td style="height: 50px; width: 50px; background-color: #767359"></td>
            <td style="height: 50px; width: 50px; background-color: #99ff99"></td>
        </tr>
    </tbody>
</table>

Detta var det första jag utformade. Med en bakgrundsbild på ett av mina favoritspel och en genomskinlig kropp gav det en rätt bra effekt för fokus på spelet. Det gröna färgschema kommer ifrån bilden ock försöker smälta in med de övriga färgerna. Eftersom det handlar mycket om överlevnad i naturen passade det bra med grönt.

##Tema2
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #996F0D"></td>
            <td style="height: 50px; width: 50px; background-color: #B18F43"></td>
            <td style="height: 50px; width: 50px; background-color: #ECB028"></td>
            <td style="height: 50px; width: 50px; background-color: #F6C75D"></td>
            <td style="height: 50px; width: 50px; background-color: #133D6E"></td>
        </tr>
    </tbody>
</table>

Detta temat fick jag inspiration ifrån avsaknaden av solen. Jag tänkte mig någon varm ort och valde en gul monokromt färgschema. Bakgrundsbilden täcker hela bakgrunden med två palmträd på var sin sida. Själva den stora bilden är tagen ifrån [Freeimages](https://www.freeimages.com/), mest för att testa deras sida.

##Tema3
<table style="border-spacing: 4px; border-collapse: separate;">
    <tbody>
        <tr style="float: left; margin-right: 50px;">
            <td style="height: 50px; width: 50px; background-color: #fff"></td>
            <td style="height: 50px; width: 50px; background-color: #000"></td>
            <td style="height: 50px; width: 50px; background-color: #008dc7"></td>
            <td style="height: 50px; width: 50px; background-color: #006994"></td>
            <td style="height: 50px; width: 50px; background-color: #FF9000"></td>
        </tr>
    </tbody>
</table>

Jag hade lite dålig fantasi för ett tredje tema. Jag utgick därför från ett av de äldre med det blåa färgschema. Fokusen låg på att använda kantlinjer och sträcka ut innehållet över hela sidan. Jag valde att inte sträcka ut själva huvuddelen av texten då det skulle bli ordentligt jobbigt att läsa. Istället är den tomma utrymmet i sidorna till för eventuella sidebars.
