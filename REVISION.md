Revision history
=======================================

v10.0.0 (2018-01-04)
-------------------------------

* Working tag for Kmom10.

v6.0.0 (2017-12-17)
-------------------------------

* Working tag for Kmom06.

v5.0.0 (2017-12-06)
-------------------------------

* Working tag for Kmom05.

v4.0.0 (2017-12-01)
-------------------------------

* Working tag for Kmom04.

v3.0.0 (2017-11-20)
-------------------------------

* Working tag for Kmom03.

v2.0.1 (2017-11-12)
---------------------------------------

* Final version for Kmom02 and better report text.


v2.0.0 (2017-11-12)
---------------------------------------

* Working project for Kmom02.


v1.1.1 (2017-11-05)
---------------------------------------

* Added testpage to test different things in anax.


v1.1.0 (2017-10-31)
---------------------------------------

* Real first release of kmom01.

v1.0.0 (2017-10-31)
---------------------------------------

* First tag, though not a working official release.
